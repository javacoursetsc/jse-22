package ru.arubtsova.tm.bootstrap;

import ru.arubtsova.tm.api.repository.ICommandRepository;
import ru.arubtsova.tm.api.repository.IProjectRepository;
import ru.arubtsova.tm.api.repository.ITaskRepository;
import ru.arubtsova.tm.api.repository.IUserRepository;
import ru.arubtsova.tm.api.service.*;
import ru.arubtsova.tm.command.AbstractCommand;
import ru.arubtsova.tm.command.authorization.*;
import ru.arubtsova.tm.command.project.*;
import ru.arubtsova.tm.command.system.*;
import ru.arubtsova.tm.command.task.*;
import ru.arubtsova.tm.command.user.UserLockByLoginCommand;
import ru.arubtsova.tm.command.user.UserRemoveByLoginCommand;
import ru.arubtsova.tm.command.user.UserUnlockByLoginCommand;
import ru.arubtsova.tm.enumerated.Role;
import ru.arubtsova.tm.exception.system.UnknownCommandException;
import ru.arubtsova.tm.repository.CommandRepository;
import ru.arubtsova.tm.repository.ProjectRepository;
import ru.arubtsova.tm.repository.TaskRepository;
import ru.arubtsova.tm.repository.UserRepository;
import ru.arubtsova.tm.service.*;
import ru.arubtsova.tm.util.TerminalUtil;

import java.util.Optional;

public class Bootstrap implements ServiceLocator {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    private final ILogService logService = new LogService();

    private final IUserRepository userRepository = new UserRepository();

    private final IUserService userService = new UserService(userRepository);

    private final IAuthService authService = new AuthService(userService);

    private void initData() {
        String testId = userService.create("test", "test", "test@test.ru").getId();
        projectService.add(testId, "Project N1", "123");
        taskService.add(testId, "Task1 4 P1", "for today");
        taskService.add(testId, "Task2 4 P1", "for tomorrow");
        taskService.add(testId, "Task3 4 P1", "for 20.02");
        String adminId = userService.create("admin", "admin", Role.ADMIN).getId();
        projectService.add(adminId, "Project MAIN", "123");
        taskService.add(adminId, "Task1 4 MAIN", "planned");
        taskService.add(adminId, "Task2 4 MAIN", "-");
        taskService.add(adminId, "Task3 4 MAIN", "obligatory");
    }

    {
        registry(new UserChangePasswordCommand());
        registry(new UserLogInCommand());
        registry(new UserLogOutCommand());
        registry(new UserRegisterCommand());
        registry(new UserUpdateProfileCommand());
        registry(new UserViewProfileCommand());

        registry(new ProjectChangeStatusByIdCommand());
        registry(new ProjectChangeStatusByIndexCommand());
        registry(new ProjectChangeStatusByNameCommand());
        registry(new ProjectClearCommand());
        registry(new ProjectCreateCommand());
        registry(new ProjectFinishByIdCommand());
        registry(new ProjectFinishByIndexCommand());
        registry(new ProjectFinishByNameCommand());
        registry(new ProjectRemoveByIdCommand());
        registry(new ProjectRemoveByIdWithTasksCommand());
        registry(new ProjectRemoveByIndexCommand());
        registry(new ProjectRemoveByNameCommand());
        registry(new ProjectShowAllCommand());
        registry(new ProjectShowByIdCommand());
        registry(new ProjectShowByIndexCommand());
        registry(new ProjectShowByNameCommand());
        registry(new ProjectStartByIdCommand());
        registry(new ProjectStartByIndexCommand());
        registry(new ProjectStartByNameCommand());
        registry(new ProjectUpdateByIdCommand());
        registry(new ProjectUpdateByIndexCommand());

        registry(new AboutCommand());
        registry(new ArgumentsListCommand());
        registry(new CommandsListCommand());
        registry(new ExitCommand());
        registry(new HelpCommand());
        registry(new SystemInfoCommand());
        registry(new VersionCommand());

        registry(new TaskBindToProjectCommand());
        registry(new TaskChangeStatusByIdCommand());
        registry(new TaskChangeStatusByIndexCommand());
        registry(new TaskChangeStatusByNameCommand());
        registry(new TaskClearCommand());
        registry(new TaskCreateCommand());
        registry(new TaskFinishByIdCommand());
        registry(new TaskFinishByIndexCommand());
        registry(new TaskFinishByNameCommand());
        registry(new TaskRemoveByIdCommand());
        registry(new TaskRemoveByIndexCommand());
        registry(new TaskRemoveByNameCommand());
        registry(new TaskShowAllByProjectIdCommand());
        registry(new TaskShowAllCommand());
        registry(new TaskShowByIdCommand());
        registry(new TaskShowByIndexCommand());
        registry(new TaskShowByNameCommand());
        registry(new TaskStartByIdCommand());
        registry(new TaskStartByIndexCommand());
        registry(new TaskStartByNameCommand());
        registry(new TaskUnbindFromProjectCommand());
        registry(new TaskUpdateByIdCommand());
        registry(new TaskUpdateByIndexCommand());

        registry(new UserLockByLoginCommand());
        registry(new UserRemoveByLoginCommand());
        registry(new UserUnlockByLoginCommand());
    }

    public void run(final String... args) {
        logService.debug("Application started");
        logService.info("*** WELCOME TO TASK-MANAGER ***");
        if (parseArgs(args)) System.exit(0);
        initData();
        while (true) {
            System.out.println("Enter Command:");
            try {
                final String command = TerminalUtil.nextLine();
                logService.command(command);
                parseCommand(command);
                System.out.println("Ok");
            } catch (final Exception e) {
                logService.error(e);
                System.out.println("Fail");
            }
        }
    }

    private void registry(final AbstractCommand command) {
        if (!Optional.ofNullable(command).isPresent()) return;
        command.setServiceLocator(this);
        commandService.add(command);
    }

    public void parseArg(final String arg) {
        if (!Optional.ofNullable(arg).isPresent() || arg.isEmpty()) return;
        final AbstractCommand command = commandService.getCommandByArg(arg);
        if (!Optional.ofNullable(command).isPresent()) return;
        command.execute();
    }

    public boolean parseArgs(final String[] args) {
        if (!Optional.ofNullable(args).isPresent() || args.length == 0) return false;
        final String arg = args[0];
        parseArg(arg);
        return true;
    }

    public void parseCommand(final String cmd) {
        if (!Optional.ofNullable(cmd).isPresent() || cmd.isEmpty()) return;
        final AbstractCommand command = commandService.getCommandByName(cmd);
        if (!Optional.ofNullable(command).isPresent()) throw new UnknownCommandException(cmd);
        final Role[] roles = command.roles();
        authService.checkRole(roles);
        command.execute();
    }

    public IAuthService getAuthService() {
        return authService;
    }

    public ICommandService getCommandService() {
        return commandService;
    }

    public IProjectService getProjectService() {
        return projectService;
    }

    public IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    public ITaskService getTaskService() {
        return taskService;
    }

    public IUserService getUserService() {
        return userService;
    }

}
