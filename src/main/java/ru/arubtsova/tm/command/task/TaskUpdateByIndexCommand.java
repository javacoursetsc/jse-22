package ru.arubtsova.tm.command.task;

import ru.arubtsova.tm.command.AbstractTaskCommand;
import ru.arubtsova.tm.exception.entity.TaskNotFoundException;
import ru.arubtsova.tm.model.Task;
import ru.arubtsova.tm.util.TerminalUtil;

import java.util.Optional;

public class TaskUpdateByIndexCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "task-update-by-index";
    }

    @Override
    public String description() {
        return "update a task by index.";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("Task:");
        System.out.println("Enter Task Index:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Optional<Task> task = serviceLocator.getTaskService().findByIndex(userId, index);
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
        System.out.println("Enter New Task Name:");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter New Task Description:");
        final String description = TerminalUtil.nextLine();
        final Optional<Task> taskUpdate = serviceLocator.getTaskService().updateByIndex(userId, index, name, description);
        Optional.ofNullable(taskUpdate).orElseThrow(TaskNotFoundException::new);
        System.out.println("Task was successfully updated");
    }

}
