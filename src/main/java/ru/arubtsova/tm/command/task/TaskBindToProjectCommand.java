package ru.arubtsova.tm.command.task;

import ru.arubtsova.tm.command.AbstractTaskCommand;
import ru.arubtsova.tm.exception.entity.TaskNotFoundException;
import ru.arubtsova.tm.model.Task;
import ru.arubtsova.tm.util.TerminalUtil;

import java.util.Optional;

public class TaskBindToProjectCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "task-bind-to-project";
    }

    @Override
    public String description() {
        return "bind task to project.";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("Enter Task Id:");
        final String taskId = TerminalUtil.nextLine();
        System.out.println("Enter Project Id:");
        final String projectId = TerminalUtil.nextLine();
        final Task task = serviceLocator.getProjectTaskService().bindTaskToProject(userId, taskId, projectId);
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
        System.out.println("Task was successfully bound to Project");
    }

}
