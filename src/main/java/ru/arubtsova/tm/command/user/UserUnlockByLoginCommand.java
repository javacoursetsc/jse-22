package ru.arubtsova.tm.command.user;

import ru.arubtsova.tm.command.AbstractCommand;
import ru.arubtsova.tm.enumerated.Role;
import ru.arubtsova.tm.util.TerminalUtil;

public class UserUnlockByLoginCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "user-unlock-by-login";
    }

    @Override
    public String description() {
        return "unlock user by login.";
    }

    @Override
    public void execute() {
        System.out.println("Unlock User:");
        System.out.println("Enter Login:");
        final String login = TerminalUtil.nextLine();
        serviceLocator.getUserService().unlockUserByLogin(login);
        System.out.println("User " + login + " was unlocked");
    }

    @Override
    public Role[] roles() {
        return new Role[]{
                Role.ADMIN
        };
    }

}
