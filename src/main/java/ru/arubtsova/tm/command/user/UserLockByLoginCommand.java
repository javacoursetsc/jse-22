package ru.arubtsova.tm.command.user;

import ru.arubtsova.tm.command.AbstractCommand;
import ru.arubtsova.tm.enumerated.Role;
import ru.arubtsova.tm.util.TerminalUtil;

public class UserLockByLoginCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "user-lock-by-login";
    }

    @Override
    public String description() {
        return "lock user by login.";
    }

    @Override
    public void execute() {
        System.out.println("Lock User:");
        System.out.println("Enter Login:");
        final String login = TerminalUtil.nextLine();
        serviceLocator.getUserService().lockUserByLogin(login);
        System.out.println("User " + login + " was locked");
    }

    @Override
    public Role[] roles() {
        return new Role[]{
                Role.ADMIN
        };
    }

}
