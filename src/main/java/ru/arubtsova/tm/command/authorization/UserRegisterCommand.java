package ru.arubtsova.tm.command.authorization;

import ru.arubtsova.tm.command.AbstractCommand;
import ru.arubtsova.tm.util.TerminalUtil;

public class UserRegisterCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "registration";
    }

    @Override
    public String description() {
        return "register a new user in task-manager.";
    }

    @Override
    public void execute() {
        System.out.println("Registration:");
        System.out.println("Enter Login:");
        final String login = TerminalUtil.nextLine();
        System.out.println("Enter Email:");
        final String email = TerminalUtil.nextLine();
        System.out.println("Enter Password:");
        final String password = TerminalUtil.nextLine();
        serviceLocator.getAuthService().register(login, password, email);
        System.out.println("You have been successfully registered");
    }

}
