package ru.arubtsova.tm.command.authorization;

import ru.arubtsova.tm.command.AbstractCommand;
import ru.arubtsova.tm.model.User;

import java.util.Optional;

public class UserViewProfileCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "view-profile";
    }

    @Override
    public String description() {
        return "show your profile information.";
    }

    @Override
    public void execute() {
        final Optional<User> user = serviceLocator.getAuthService().getUser();
        System.out.println("Profile Overview:");
        System.out.println("Login: " + user.get().getLogin());
        System.out.println("Email: " + user.get().getEmail());
        System.out.println("First Name: " + user.get().getFirstName());
        System.out.println("Last Name: " + user.get().getLastName());
        System.out.println("Middle Name: " + user.get().getMiddleName());
    }

}
