package ru.arubtsova.tm.command.authorization;

import ru.arubtsova.tm.command.AbstractCommand;

public class UserLogOutCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "logout";
    }

    @Override
    public String description() {
        return "log you out of the system.";
    }

    @Override
    public void execute() {
        System.out.println("Logout:");
        serviceLocator.getAuthService().logout();
        System.out.println("You have been successfully logged out");
    }

}
