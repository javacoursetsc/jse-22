package ru.arubtsova.tm.api;

import ru.arubtsova.tm.model.AbstractEntity;

import java.util.List;
import java.util.Optional;

public interface IRepository<E extends AbstractEntity> {

    List<E> findAll();

    void add(E entity);

    Optional<E> findById(String id);

    void clear();

    E removeById(String id);

    void remove(E entity);

}
