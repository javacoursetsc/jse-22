package ru.arubtsova.tm.api.service;

import ru.arubtsova.tm.api.IBusinessService;
import ru.arubtsova.tm.model.Task;

public interface ITaskService extends IBusinessService<Task> {

    Task add(String userId, String name, String description);

}
