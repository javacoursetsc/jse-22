package ru.arubtsova.tm.repository;

import ru.arubtsova.tm.api.repository.ITaskRepository;
import ru.arubtsova.tm.exception.entity.TaskNotFoundException;
import ru.arubtsova.tm.model.Task;

import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class TaskRepository extends AbstractBusinessRepository<Task> implements ITaskRepository {

    public static Predicate<Task> predicateByProjectId(final String projectId) {
        return s -> projectId.equals(s.getProjectId());
    }

    @Override
    public List<Task> findAllByProjectId(final String userId, final String projectId) {
        return map.values().stream()
                .filter(predicateByUserId(userId))
                .filter(predicateByProjectId(projectId))
                .collect(Collectors.toList());
    }

    @Override
    public void removeAllByProjectId(final String userId, final String projectId) {
        findAllByProjectId(userId, projectId).forEach(task -> map.remove(task));
    }

    @Override
    public Task bindTaskToProject(final String userId, final String taskId, final String projectId) {
        Optional<Task> task = findById(userId, taskId);
        task.ifPresent(t -> t.setProjectId(projectId));
        task.orElseThrow(TaskNotFoundException::new);
        return task.orElse(null);
    }

    @Override
    public Task unbindTaskFromProject(final String userId, final String taskId) {
        Optional<Task> task = findById(userId, taskId);
        task.ifPresent(t -> t.setProjectId(null));
        task.orElseThrow(TaskNotFoundException::new);
        return task.orElse(null);
    }

}
