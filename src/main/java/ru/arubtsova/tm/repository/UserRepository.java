package ru.arubtsova.tm.repository;

import ru.arubtsova.tm.api.repository.IUserRepository;
import ru.arubtsova.tm.model.User;

import java.util.Optional;
import java.util.function.Predicate;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    public static Predicate<User> predicateByLogin(final String login) {
        return u -> login.equals(u.getLogin());
    }

    public static Predicate<User> predicateByEmail(final String email) {
        return u -> email.equals(u.getEmail());
    }

    @Override
    public Optional<User> findByLogin(final String login) {
        return map.values().stream()
                .filter(predicateByLogin(login))
                .limit(1)
                .findFirst();
    }

    @Override
    public Optional<User> findByEmail(final String email) {
        return map.values().stream()
                .filter(predicateByEmail(email))
                .limit(1)
                .findFirst();
    }

    @Override
    public User removeByLogin(final String login) {
        final Optional<User> entity = findByLogin(login);
        entity.ifPresent(this::remove);
        return entity.orElse(null);
    }

}
