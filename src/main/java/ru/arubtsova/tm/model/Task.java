package ru.arubtsova.tm.model;

import ru.arubtsova.tm.api.entity.IWBS;

public class Task extends AbstractBusinessEntity implements IWBS {

    private String projectId;

    public Task() {
    }

    public Task(String name) {
        this.name = name;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

}
